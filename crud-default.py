from flask import Flask,request,jsonify
import  pymysql
app = Flask(__name__)
conn=pymysql.connect( host='localhost',
                      user= 'root',
                      password='',
                      database='employee')
@app.route('/insert_query',methods=['POST'])
def insert_qry():
    if request.method=='POST':
        emp_id = request.form['emp_id']
        name = request.form['name']
        salary_grade = request.form['salary_grade']
        address = request.form['address']
        cursor = conn.cursor()
        cursor.execute(''' INSERT INTO employee_table VALUES(%s,%s,%s,%s)''', (emp_id, name, salary_grade, address))
        conn.commit()
        cursor.close()
        return f'success'
@app.route('/select_query',methods=['GET'])
def select_query():
    if request.method=='GET':
        table_name=request.form['table_name']
        cursor=conn.cursor()
        query="SELECT * FROM "+table_name
        cursor.execute(query)#('''SELECT * FROM (%s)''',(table_name))
        result=cursor.fetchall()
        cursor.close()
        return jsonify({'data':result})
@app.route('/update_query',methods=['POST','GET'])
def update_query():
    if request.method=='POST':
        emp_id = request.form['emp_id']
        name = request.form['name']
        #salary_grade = request.form['salary_grade']
        #address = request.form['address']
        cursor=conn.cursor()
        query='''UPDATE employee_table SET name= {0} WHERE emp_id={1}'''.format(name,emp_id)
        cursor.execute(query)
        conn.commit()
        cursor.close()
        return f'ok'
@app.route('/delete_query',methods=['DELETE'])
def delete_query():
    if request.method=='DELETE':
        emp_id=request.form['emp_id']
        cursor=conn.cursor()
        query="DELETE FROM employee_table WHERE emp_id={}".format(emp_id)
        cursor.execute(query)
        conn.commit()
        cursor.close()
        return f'deleted successfully'
app.run()