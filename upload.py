from flask import Flask,request,jsonify
import os
from werkzeug.utils import secure_filename
UPLOAD_FOLDER='E:\qfiles'
app=Flask(__name__)
app.config['UPLOAD_FOLDER']=UPLOAD_FOLDER
ext=['doc','docx']
def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ext
@app.route('/upload',methods=['POST'])
def upload_file():
    if request.method=='POST':
        if 'file' not in  request.files:
            return jsonify({'message':'no file '})
        file=request.files['file']
        if file.filename== '':
            return jsonify({'message':'error no file'})
        if file and allowed_file(file.filename):
            filename=secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            return jsonify({'message':'success'})
        else:
            return jsonify({'message':'allowed file type .docx'})
    else:
        return 'error'
if __name__ == '__main__':
    print(os.path.join(app.config['UPLOAD_FOLDER'], 'dsfd.exe'))
    app.run()