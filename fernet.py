from cryptography.fernet import Fernet
def data_encryption(message):
    key = Fernet.generate_key() #can be set manually restricted to 44 bytes
    f = Fernet(key)
    enc_data = f.encrypt(message.encode())
    print(enc_data.decode())
    dec_data = f.decrypt(enc_data)
    print(dec_data.decode())
data_encryption("bldwe12342w")
