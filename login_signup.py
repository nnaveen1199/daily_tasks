from flask import Flask,request,jsonify
from flask_mysqldb import  MySQL
import binascii
import hashlib
import os
app=Flask(__name__)
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = ''
app.config['MYSQL_DB'] = 'data'

mysql=MySQL(app)



INSERT='INSERT'
SELECT='SELECT'
UPDATE='UPDATE'
DELETE='DELETE'
SELECTALL='SELECTALL'



@app.route('/api/signup',methods=['POST'])
def signup():
    data=request.get_json()
    user_name=data.get('user_name')
    email_id=data.get('email_id')
    password=data.get('password')
    mobile=data.get('mobile')
    if len(data)>4:
        return jsonify({"message":"unwanted data has been provided"})
    if not user_name or not email_id or not password or not mobile :
        return jsonify({'Message':'Pleaase enter all details'})
    if len(password)<8:
        return  jsonify({"message":"password must be atleast 8 digits long"})
    if "@" not in email_id:
        return jsonify({"message":"please enter valid email_id"})
    if "." not in email_id:
        return jsonify({"message": "please enter valid email_id"})
    validate = email_id.split('.')[1]
    if len(validate) != 3:
        return jsonify({"message": "enter valid mail id"})
    mob=str(mobile)
    if len(mob)!= 10:
        return jsonify({"message":"mobile number should be 10 digit"})
    cur=mysql.connection.cursor()
    qry= "SELECT user_id FROM signup WHERE email_id='{0}' OR mobile={1}".format(email_id,mobile)
    cur.execute(qry)
    user_exists=cur.fetchall()
    if user_exists:
        return jsonify({"message":"An user already exists with an entered email_id or mobile_id as user_id {}".format(user_exists)})
    password_en=_hash_password(password)
    data.update({"password":password_en})
    query=insert_query("signup",data)
    result=run_query(INSERT,query)
    if result is None:
        return jsonify({"message":"error while executing query"})
    query2 = "SELECT user_id FROM signup where email_id= '{}' ".format(email_id)
    id1=run_query(SELECT,query2)
    id=id1[0]
    query1="SELECT user_id,user_name,email_id,mobile FROM signup WHERE user_id={}".format(id)
    data1=run_query(SELECT, query1)
    col = ['id', 'name', 'email', 'mobile']
    _res = {}
    for i in range(4):
        #col = data.get()
        _res.update({col[i]: data1[i]})
    return jsonify({"value": _res})
    #return jsonify({"ID":data1[0][0],"NAME":data1[0][1],"EMAIL":data1[0][2],"password":data1[0][3],"mobile":data1[0][4]})

@app.route('/api/login',methods=['POST'])
def login():
    data=request.get_json()
    email_id=data.get('email_id')
    if "@" not in email_id:
        return jsonify({"message":"please enter valid email_id"})
    if "." not in email_id:
        return jsonify({"message": "please enter valid email_id"})
    validate = email_id.split('.')[1]
    if len(validate) != 3:
        return jsonify({"message": "enter valid mail id"})
    provided_password=data.get('password')
    query= "SELECT password FROM signup WHERE email_id= '{}' ".format(email_id)
    stored_password1=run_query(SELECT,query)
    print(stored_password1)

    if not stored_password1:
        return jsonify({"message":"please check the details entered"})
    stored_password = stored_password1[0]
    print(stored_password)
    if _verify_password(stored_password,provided_password):
        query2="SELECT user_id FROM signup where email_id= '{}' ".format(email_id)
        id=run_query(SELECT,query2)
        id1=id[0]
        query1="SELECT user_id,user_name,email_id,mobile FROM signup WHERE user_id={} ".format(id1)
        data1=run_query(SELECT,query1)
        col = ['id', 'name', 'email', 'mobile']
        _res = {}
        for i in range(4):
            #col = data.get()
            _res.update({col[i]: data1[i]})
        return jsonify({"value": _res})
    else:
        return jsonify({"message":"incorrect password"})
        #return jsonify({"ID": data1[0][0], "NAME": data1[0][1], "EMAIL": data1[0][2], "password": data1[0][3],"mobile": data1[0][4]})




@app.route('/api/user/<id>',methods=['POST'])
def update_user(id):
    query = "SELECT * FROM signup WHERE user_id={}".format(id)
    _data = run_query(SELECT, query)
    if not _data:
        return jsonify({"message": "user does not exist with the provided id"})
    data=request.get_json()
    if len(data)==0:
        return jsonify({"message":"no data provided"})
    if len(data)>4:
        return jsonify({"message":"column length exceeded"})
    col=["user_name","email_id","password","mobile"]
    for key in data:
        if key not in col:
            return  jsonify({"message":"enter vallid column name"})
    #if "user_name" in data or "email_id" in data or "password" in data or "mobile" in data:
        #return jsonify({"message":"invalid column data has been provided"})
    if "email_id" in data:
        email_id=data.get('email_id')
        if len(email_id)<5:
            return jsonify({"message":"email_id does not meet the requirements"})
        if "@" not in email_id:
            return jsonify({"message": "email_id misses @ symbol"})
        if "." not in email_id:
            return jsonify({"message": "please enter valid email_id"})
        validate=email_id.split('.')[1]
        if len(validate) !=3:
            return jsonify({"message":"enter valid mail id"})
        _query="SELECT user_id FROM signup WHERE email_id='{}' ".format(email_id)
        res_=run_query(SELECT,_query)
        print(res_)
        if res_:
            return jsonify({"message":"An user already exists with the provided email_id"})
    if "user_name" in data:
        user_name=data.get('user_name')
        if len(user_name) == 0:
            return jsonify({"message":"username can't be empty"})
    if "mobile" in data:
        mobile=data.get('mobile')
        mob=str(mobile)
        if len(mob)!=10:
            return jsonify({"message":"Please enter 10 digit mobile number"})
        _query = "SELECT user_id FROM signup WHERE mobile='{}' ".format(mobile)
        res_ = run_query(SELECT, _query)
        print(res_)
        if res_:
            return jsonify({"message": "An user already exists with the provided mobile"})
    if "password" in data:
        password=data.get('password')
        if len(password)<8:
            return jsonify({"message":"password must be atleast 8 char long"})
        password_en = _hash_password(password)
        data.update({"password": password_en})
    qry=update_query("signup",data,id)
    affected=run_query(UPDATE,qry)
    print (affected)
    if not affected:
        return  jsonify({"Message":"error while executing query"})
    query="SELECT user_id,user_name,email_id,mobile FROM signup WHERE user_id={}".format(id)
    data1=run_query(SELECT,query)
    col = ['id', 'name', 'email', 'mobile']
    _res = {}
    for i in range(4):
        #col = data.get()
        _res.update({col[i]: data1[i]})
    return jsonify({"value": _res})
    #return jsonify({"ID": data1[0][0], "NAME": data1[0][1], "EMAIL": data1[0][2], "password": data1[0][3], "mobile": data1[0][4]})
def len_mobile(mobile):
    data=request.get_json()
    if "mobile" in data:
        mob=str(mobile)
        if len(mob)==10:
            return 1

@app.route('/api/read/<id>',methods=['GET'])
def read(id):
    query="SELECT user_id,user_name,email_id,mobile FROM signup WHERE user_id={}".format(id)
    data1=run_query(SELECT,query)
    print(data1)
    if data1==0:
        return jsonify({"message":"entered user id is not available"})
    col = ['id', 'name', 'email', 'mobile']
    _res = {}
    for i in range(4):
        # col = data.get()
        _res.update({col[i]: data1[i]})
    return jsonify({"value": _res})
@app.route('/api/all_users',methods=['GET'])
def read1():
    query="SELECT user_id,user_name,email_id,mobile FROM signup"
    data1=run_query(SELECTALL,query)
    print(len(data1))
    if data1==0:
        return jsonify({"message":"entered user id is not available"})
    col = ['id', 'name', 'email', 'mobile']
    #_res = {}
    res={}
    #l=4*19
    #for i in range(l):
        #col = data.get()[0]
    for i in range(len(data1)):
        _res={}
        for j in range(len(col)):
            _res[col[j]] =data1[i][j]
            print(_res)
        res.update({i:_res})
        #_res.update({col[i]: data1[i]})
    return jsonify({"value": res})

@app.route('/api/user_delete/<id>',methods=['DELETE'])
def delete_user(id):
    query = "SELECT * FROM signup WHERE user_id={}".format(id)
    data = run_query(SELECT, query)
    if not data:
        return jsonify({"message":"user does not exist with the provided id"})
    qry="DELETE FROM signup WHERE user_id={}".format(id)
    affected=run_query(DELETE,qry)
    if affected:
        return jsonify({"message":"User deleted successfully . Signup to proceed"})
    return  jsonify({"message":"query didn't get executed"})


def _hash_password(password):
    salt = hashlib.sha256(os.urandom(60)).hexdigest().encode('ascii')
    pwdhash = hashlib.pbkdf2_hmac('sha512', password.encode('utf-8'), salt, 100000)
    pwdhash = binascii.hexlify(pwdhash)
    return (salt + pwdhash).decode('ascii')
def _verify_password(stored_password, provided_password):
    salt = stored_password[:64]
    stored_password = stored_password[64:]
    pwdhash = hashlib.pbkdf2_hmac('sha512', provided_password.encode('utf-8'), salt.encode('ascii'), 100000)
    pwdhash = binascii.hexlify(pwdhash).decode('ascii')
    print(pwdhash)
    print(stored_password)
    return pwdhash == stored_password

def insert_query(table_name,data):
    qry="INSERT INTO "+ table_name +" ({0}) VALUES ({1})"
    cols=''
    values=''
    for key in data:
        cols += "{},".format(key)
        values +="'{}',".format(data[key])
    _qry=qry.format(cols[:-1],values[:-1])
    return _qry


def update_query(table_name, data,id):
    qry = "UPDATE " + table_name + " SET {} WHERE user_id={}"
    update = ''
    for key in data:
        if data[key] != "None" and data[key] != "null" and data[key] != "NULL" and data[key] != "undefined":
            update += " {}=\"{}\",".format(key, data[key])
    if not len(update):
        return None
    result=qry.format(update[:-1],id)
    return result



def run_query(query_type, query):
    print(query)
    cur=mysql.connection.cursor()
    if query_type is not None and (query_type in (INSERT,UPDATE,DELETE)):
        cur.execute(query)
        mysql.connection.commit()
        affected= cur.rowcount
        cur.close()
        return affected
    elif query_type is not None or query_type== SELECTALL:
        cur=mysql.connection.cursor()
        cur.execute(query)
        result=cur.fetchall()
        return result
    elif query_type is not None or query_type== SELECT:
        cur=mysql.connection.cursor()
        cur.execute(query)
        result=cur.fetchall()
        print(result)
        if not result:
            return 0
        cur.close()
        print (result)
        return result[0]
app.run()
